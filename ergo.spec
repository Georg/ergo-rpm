#
# spec file for package ergo
#
# Copyright (c) 2021 SUSE LLC
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#


%define __arch_install_post export NO_BRP_STRIP_DEBUG=true
%global provider_prefix github.com/ergochat/ergo
%global import_path %{provider_prefix}
%if 0%{?fedora}
%global goipath github.com/ergochat/ergo
%global debug_package %{nil}
%endif
Name:           ergo
Version:        2.9.1
Release:        0
Summary:        IRCv3 Server
License:        MIT
Group:          Productivity/Networking/IRC
URL:            https://ergo.chat/
#Original source:
#https://github.com/ergochat/ergo/tree/master
#Modified source (custom ircd.yaml configuration and systemd unit file):
Source0:        %{name}-%{version}.tar.gz
Source1:        vendor.tar.gz
Patch0:         default.yaml.patch
Patch1:         ergo.service.patch
# Dependencies for openSUSE and SLES:
%if 0%{?suse_version}
BuildRequires:  fdupes
BuildRequires:  go >= 1.18
BuildRequires:  golang-packaging
BuildRequires:  systemd
BuildRequires:  systemd-rpm-macros
Requires(pre):  %{_bindir}/getent
Requires(pre):  %{_sbindir}/useradd
%endif
# Dependencies for Fedora:
%if 0%{?fedora}
BuildRequires:  fdupes
BuildRequires:  go-rpm-macros
BuildRequires:  golang >= 1.17
BuildRequires:  systemd
BuildRequires:  systemd-rpm-macros
Requires(pre):  %{_bindir}/getent
Requires(pre):  %{_sbindir}/useradd
%endif
%if 0%{?suse_version}
%{go_nostrip}
%{go_provides}
%endif
%if 0%{?fedora}
%gopkg
%endif

%description
Ergo is a modern IRC server written in Go.

%pre
%{_bindir}/getent group ergo > /dev/null || %{_sbindir}/groupadd -r ergo
%{_bindir}/getent passwd ergo > /dev/null || %{_sbindir}/useradd -r -g ergo -d %{_sysconfdir}/ergo -s /sbin/nologin ergo
%if 0%{?suse_version}
%service_add_pre ergo.service
%endif

%prep
%setup -q

%patch0 -p1
%patch1 -p1

%if 0%{?fedora}
%goprep
%endif
%setup -q -T -D -a 1

%build
export GOFLAGS="-mod=vendor"
%if 0%{?suse_version}
%goprep %{import_path}
%gobuild .
%endif

%if 0%{?fedora}
rm -rf "%{_builddir}/%{extractdir0}/_build/src/github.com/ergochat/ergo"
ln -s "%{_builddir}/%{name}-%{version}" "%{_builddir}/%{extractdir0}/_build/src/github.com/ergochat/ergo"
%gobuild -o %{gobuilddir}/bin/%{name} %{goipath}/irc
%endif

%install
%if 0%{?suse_version}
%goinstall
%gosrc
%endif

%if 0%{?fedora}
install -m 0755 -vd %{buildroot}%{_bindir}
install -m 0755 -vp %{gobuilddir}/bin/%{name} %{buildroot}%{_bindir}/
%endif

install -d -m 755 %{buildroot}%{_sysconfdir}/%{name}
install -d -m 700 %{buildroot}%{_sharedstatedir}/%{name}
install -D -m 660 %{_builddir}/%{name}-%{version}/default.yaml %{buildroot}%{_sysconfdir}/%{name}/ircd.yaml
install -D -m 644 %{_builddir}/%{name}-%{version}/distrib/systemd/%{name}.service %{buildroot}%{_unitdir}/%{name}.service
install -d -m 755 %{buildroot}%{_sbindir}

%check
%if 0%{?suse_version}
%gotest %{import_path}
%endif

%if 0%{?fedora}
%gocheck
%endif

%if 0%{?suse_version}
%gofilelist
%files -f file.lst
%endif

%if 0%{?fedora}
%files
%endif

%license LICENSE
%doc README.md
%attr(755,ergo,ergo) %{_bindir}/%{name}
%dir %attr (755,ergo,ergo) %{_sysconfdir}/%{name}
%dir %attr (700,ergo,ergo) %{_sharedstatedir}/%{name}
%attr (-,ergo,ergo) %config(noreplace) %{_sysconfdir}/%{name}/ircd.yaml
%{_unitdir}/%{name}.service

%post
%if 0%{?suse_version}
%service_add_post ergo.service
%endif
%if 0%{?fedora}
%systemd_post ergo.service
%endif
install -d -o ergo -g ergo -m 750 %{_sysconfdir}/ssl/ergo
if [ $1 -eq 1 ]
then
    if %{_bindir}/%{name} mkcerts --conf %{_sysconfdir}/%{name}/ircd.yaml --quiet
    then
        chown ergo:ergo %{_sysconfdir}/ssl/%{name}/crt
        chown ergo:ergo %{_sysconfdir}/ssl/%{name}/key
        echo "Generated a self signed certificate. Please replace /etc/ssl/%{name}/crt and /etc/ssl/%{name}/key for production use."
    fi
fi
if [ -f %{_sharedstatedir}/%{name}/ircd.db ]
then
    %{_bindir}/%{name} upgradedb --conf %{_sysconfdir}/%{name}/ircd.yaml
fi
if [ ! -f %{_sharedstatedir}/%{name}/ircd.db ]
then
    if %{_bindir}/%{name} initdb --conf %{_sysconfdir}/%{name}/ircd.yaml --quiet
    then
        chown ergo:ergo %{_sharedstatedir}/%{name}/ircd.db
        chmod 600 %{_sharedstatedir}/%{name}/ircd.db
    fi
fi
if [ $1 -eq 1 ]
then
    echo "Welcome to Ergo!"
    echo "To enable the service (auto-start): \`sudo systemctl enable ergo\`"
    echo "To start the service now: \`sudo systemctl start ergo\`"
    echo "The configuration is located in %{_sysconfdir}/ergo/ircd.yaml"
fi
if [ $1 -eq 2 ]
then
    echo "Restart IRC daemon using \`systemctl restart ergo\` to complete upgrade."
fi

%preun
%if 0%{?suse_version}
%service_del_preun ergo.service
%endif
%if 0%{?fedora}
%systemd_preun ergo.service
%endif

%postun
#%if 0%{?suse_version}
#%service_del_postun_without_restart ergo.service
#%endif
%if 0%{?fedora}
%systemd_postun ergo.service
%endif

%changelog
